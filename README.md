# DetekcijaSemaforaV2

Ovaj kod je namijenjen detekciji semafora na videu. Koristi se biblioteka OpenCV (cv2) za obradu slika i video materijala.


## Instalacija

Prije pokretanja koda, potrebno je instalirati OpenCV biblioteku. Možete je instalirati pomoću pip naredbe:
**pip install opencv-python**


## Korištenje

1. Stavite video datoteku "DayDrive2.mp4" u isti direktorij kao i ovaj kod.
2. Pokrenite kod u Python okruženju.
**python semafor_detekcija.py**

1. Otvara se prozor sa snimkom.
2. Detektirani semafori će biti označeni zelenim krugovima.
3. Možete koristiti tipke na tipkovnici za kontrolu reprodukcije:
    - 'q': Izlazak iz programa.
    - 'f': Premotavanje 150 okvira unaprijed.
    - 'd': Premotavanje 150 okvira unatrag.


## Algoritam

1. Učitavanje video datoteke "DayDrive2.mp4".
2. Postavljanje trenutne pozicije okvira na početak.
3. Petlja za obradu svakog okvira u video snimku.
4. Izdvajanje regije interesa (ROI) koja sadrži semafore na okviru.
5. Konverzija slike u HSV prostor.
6. Prepoznavanje semafora korištenjem definiranih raspona boja u HSV prostoru (crvena, žuta, zelena).
7. Filtriranje kontura prema površini kako bi se uklonile nepoželjne konture.
8. Pronalaženje centra i radijusa konture kako bi se dobila pozicija i veličina semafora.
9. Provjera da li je kontura bliska krugu kako bi se izbjegle lažne detekcije.
10. Označavanje semafora na slici zelenim krugovima i tekstom "Semafor".
11. Prikazivanje slike sa označenim semaforima.
12. Provjera korisničkih akcija za izlazak ili kontrolu reprodukcije video snimka.
13. Oslobađanje resursa i zatvaranje prozora.

**!!Molim vas da imate na umu da je ovaj kod samo primjer i da se može prilagoditi vašim specifičnim potrebama ili okruženju.!!**



