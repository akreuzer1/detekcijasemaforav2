import cv2 as cv
import numpy as np

# Parametri bojenog prostora HSV za crveni, žuti i zeleni semafor
hsv_ranges = [
    ((0, 100, 100), (10, 255, 255)),   # Crvena boja
    ((20, 100, 100), (40, 255, 255)),  # Žuta boja
    ((50, 100, 100), (163, 255, 255))  # Zelena boja
]

cap = cv.VideoCapture("DayDrive2.mp4")
frame_pos = 0

while(cap.isOpened()):
    cap.set(cv.CAP_PROP_POS_FRAMES, frame_pos)
    ret, frame = cap.read()

    if ret:
        # Izdvajanje ROI-a
        roi = frame[50:frame.shape[0]//1, 0:frame.shape[1]]

        # Konverzija u HSV bojeni prostor
        hsv = cv.cvtColor(roi, cv.COLOR_BGR2HSV)

        # Prepoznavanje semafora
        detected_circles = []
        for i, (lower, upper) in enumerate(hsv_ranges):
            mask = cv.inRange(hsv, lower, upper)
            contours, _ = cv.findContours(mask, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)

            for contour in contours:
                area = cv.contourArea(contour)
                if 200 < area < 5000:  # Filtriranje kontura prema površini

                    # Pronalaženje centra i radijusa konture
                    (x, y), radius = cv.minEnclosingCircle(contour)
                    center = (int(x), int(y))
                    radius = int(radius)

                    # Provjera je li kontura bliska krugu
                    circularity = cv.contourArea(contour) / (np.pi * radius ** 2)
                    circularity_threshold = 0.7   # Prag za prepoznavanje okruglih kontura
                    if circularity > circularity_threshold:
                        detected_circles.append((center, radius))

        # Označavanje semafora na slici
        for center, radius in detected_circles:
            cv.circle(roi, center, radius, (0, 255, 0), 2)
            cv.putText(roi, "Semafor", (center[0] - radius, center[1] - radius - 10),
                       cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

        # Prikazivanje slike
        cv.imshow('Frame', roi)

        # Provjera za izlazak iz petlje
        key = cv.waitKey(25)
        if key & 0xFF == ord('q'):
            break
        elif key & 0xFF == ord('r'):
            frame_pos = max(frame_pos - 10, 0)  # rewind by 10 frames
        elif key & 0xFF == ord('f'):
            frame_pos += 150  # forward by 150 frames
        elif key & 0xFF == ord('d'):
            frame_pos -= 150  # forward by 150 frames
    else:
        break

    frame_pos += 1

cap.release()
cv.destroyAllWindows()
